# Translation Text for Scoutnet Kalender Module.
# Copyright (C) 2020 ScoutNet
# This file is distributed under the same license as the scoutnet_kalender package.
# Stefan "Mütze" Horst <muetze@scoutnet.de>, 2020.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: scoutnet_kalender\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-06-29 22:19+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"


#: templates/snk-block.html.twig:26
#: templates/snk-full-view.html.twig:91
#: templates/snk-event.html.twig:27
msgid "add event"
msgstr ""

#: templates/snk-full-view.html.twig:7
msgid "stammesAuswahl"
msgstr ""

#: templates/snk-full-view.html.twig:17
msgid "categoryAuswahl"
msgstr ""

#: templates/snk-full-view.html.twig:31
msgid "ebene"
msgstr ""

#: templates/snk-full-view.html.twig:32
#: templates/snk-event.html.twig:4
msgid "date"
msgstr ""

#: templates/snk-full-view.html.twig:33
#: templates/snk-event.html.twig:7
msgid "time"
msgstr ""

#: templates/snk-full-view.html.twig:34
#: templates/snk-event.html.twig:10
msgid "titel"
msgstr ""

#: templates/snk-full-view.html.twig:35
#: templates/snk-event.html.twig:13
msgid "section"
msgstr ""

#: templates/snk-full-view.html.twig:36
#: templates/snk-event.html.twig:15
msgid "categories"
msgstr ""

#: templates/snk-full-view.html.twig:73
#: templates/snk-event.html.twig:18
msgid "description"
msgstr ""

#: templates/snk-full-view.html.twig:74
#: templates/snk-event.html.twig:19
msgid "location"
msgstr ""

#: templates/snk-full-view.html.twig:75
#: templates/snk-event.html.twig:20
msgid "organizer"
msgstr ""

#: templates/snk-full-view.html.twig:76
#: templates/snk-event.html.twig:21
msgid "targetGroup"
msgstr ""

#: templates/snk-full-view.html.twig:77
#: templates/snk-event.html.twig:22
msgid "URL"
msgstr ""

#: templates/snk-full-view.html.twig:78
#: templates/snk-event.html.twig:23
msgid "author"
msgstr ""




#: src/Plugin/Block/KalenderBlock.php:85
msgid 'Kalender SSIDs'
msgstr ""

#: src/Plugin/Block/KalenderBlock.php:86
msgid 'Which Calendars do you want to show? Seperated by comma.'
msgstr ""

#: src/Plugin/Block/KalenderBlock.php:92
msgid 'Optional Kalender SSIDs'
msgstr ""

#: src/Plugin/Block/KalenderBlock.php:93
msgid 'Which Calendars do you want the User to choose from? Seperated by comma.'
msgstr ""

#: src/Plugin/Block/KalenderBlock.php:99
msgid 'Kalender Category IDs'
msgstr ""

#: src/Plugin/Block/KalenderBlock.php:100
msgid 'Which Categories do you want to show? Seperated by comma.'
msgstr ""

#: src/Plugin/Block/KalenderBlock.php:105
msgid 'Optional Kalender Category IDs'
msgstr ""

#: src/Plugin/Block/KalenderBlock.php:106
msgid 'Which Categories do you want the User to choose from? Seperated by comma.'
msgstr ""

#: src/Plugin/Block/KalenderBlock.php:112
msgid 'Nr. of Events'
msgstr ""

#: src/Plugin/Block/KalenderBlock.php:113
msgid 'How many Events do you want to show?'
msgstr ""
