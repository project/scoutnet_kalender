# ScoutnetKalender Modul für Drupal
Offizielles Drupal Module für den ScoutNet.de Kalender. 

Dieses Modul bietet die möglichkeit den ScoutNet.de Kalender in die eigene Drupal Seite einzubinden. Dazu wird eine Api verwendet.

Es gibt sowohl einen konfigurierbaren Block, als auch die möglichkeit über ein routing einen kalender anzuzeigen.

## Einbindung
Zum installieren einfach 

`composer require 'drupal/scoutnet_kalender:^1.0'` 

verwenden

### Block
Der Block wird per Drupal eingebunden und kann konfiguriert werden. Es muss die ScoutnetID (kurz SSID) des Kalenders angeben werden.
Damit auch mehr als ein Kalender gleichzeitig angezeigt werden kann, kann man in das SSID Feld per , seperiert mehrere SSIDs eingeben.

Diese Liste von Terminen kann anhand der Kategorie IDs gefiltert werden. Auch diese kann als komma seprerierte Liste eingegeben werden.

Abschliessend ist noch die Anzahl der angezeigten Termine zu definieren. Per default werden hier 3 Terime Angezeigt.

### Seite
Zusätzlich existiert noch ein Routing, um sowohl ganze Kalender anzuzeigen, als auch nur einzelne Termine. Die URLs lauten:

```/snk/calendar/{ssid}/{categories}```

Wobei die SSID wieder eine komma seperierte liste sein kann, und die Kategories auch leer sein koennen. Ein paar beispiele lauten:

```url
Termine des DPSG Bundesverband: /snk/calendar/3
Termine von ScoutNet: /snk/calendar/2195,2220/238,188,239,242
```

Es existiert auch die Möglichkeit dem Benutzer weiter Kalender bzw. Kategorien zur Auswahl anzubieten. Hierzu wird hinter die SSID noch per : eine weitere Liste angegeben:

```url
Termine des DPSG Bundesverband mit zusätzlichem Anzeigen von der DPSG Diozese Köln, und deb DPSG Bezirk Erft:
    /snk/calendar/3:4,17
Termine des DPSG Bundesverband und aller DPSG Diözesen mit der Option alle Termine Ausbildungs und Schulungs Kategorien anzuzeigen: 
    /snk/calendar/3:14,26,161,20,98,246,32,115,30,264,646,2212,241,170,84,116,16,310,136,62,46,49,4,35,53/10:476,179,331,330,477,865,478,479,333,480,820,332,328,481,483,484,485,36,486,701,702,489,897
```

## Design Anpassen
Alle standartmäßig generierten Templates nutzen CSS, womit nur kleine Design änderungen sehr einfach möglich sind. Wenn jedoch weitere änderungen gewünscht sind,
so gibt es die möglichkeit die vorhandenen twig Templates anzupassen. Dazu existierten drei Templates:

- snk-block.html.twig: Design des Blockes
- snk-event.html.twig: Design einzelner Termine
- snk-full-view.html.twig: Design der full view Tabelle
