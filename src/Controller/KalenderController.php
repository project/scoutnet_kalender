<?php
/**
 ************************************************************************
 * Copyright (c) 2005-2020 Stefan (Muetze) Horst                        *
 ************************************************************************
 * I don't have the time to read through all the licences to find out   *
 * what the exactly say. But it's simple. It's free for non commercial  *
 * projects, but as soon as you make money with it, i want my share :-) *
 * (License : Free for non-commercial use)                              *
 ************************************************************************
 * Authors: Stefan (Muetze) Horst <muetze@DPSG-Liblar.de>               *
 ************************************************************************
 */


namespace Drupal\scoutnet_kalender\Controller;

use Drupal\Core\Controller\ControllerBase;
use ScoutNet\Api\ScoutnetApi;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class KalenderController extends ControllerBase {
    public function content() {
        return [
            '#type' => 'markup',
            '#markup' => 'Hello World',
        ];
    }
    /**
     * Display the markup.
     *
     * @return array
     *   Return markup array.
     */
    public function calendar($ssid, $categories) {
        $optional_ssids = [];
        if (strpos($ssid, ':') !== False) {
            list($ssid, $opt) = explode(':', $ssid, 2);
            $optional_ssids = array_map('intval', array_map('trim', explode(',', $opt)));
        }
        $ssids = array_map('intval', array_map('trim', explode(',', $ssid ?? '2195,2220')));
        $addids = array_map('intval', $_POST['addids'] ?? []);
        $ssids = array_merge($ssids, $addids);

        $optional_cats = [];
        $cats = [];

        if (trim($categories ?? '')) {
            if (strpos($categories, ':') !== False) {
                list($categories, $opt) = explode(':', $categories, 2);
                $optional_cats = array_map('intval', array_map('trim', explode(',', $opt)));
            }
            $cats = array_map('intval', array_map('trim', explode(',', $categories ?? '')));
            $addcats = array_map('intval', $_POST['addcats'] ?? []);
            $cats = array_merge($cats, $addcats);
        }


        $filter = [
            'limit' => 30, // intval($this->configuration['snk_block_limit'] ?? '30'),
            'after' => 'now()',
        ];

        if (count($cats) > 0) {
            $filter['kategories'] = $cats;
        }

        $scoutNetApi = new ScoutnetApi();
        $events = $scoutNetApi->get_events_for_global_id_with_filter($ssids, $filter);
        $structures = $scoutNetApi->get_kalender_by_global_id($ssids);

        $optional_structures = [];
        if (count($optional_ssids) > 0) {
            foreach ($scoutNetApi->get_kalender_by_global_id($optional_ssids) as $optStructure) {
                $optional_structures[] = [
                    'selected' => in_array($optStructure->getUid(), $addids),
                    'structure' => $optStructure,
                ];
            }
        }
        $optional_categories = [];
        if (count($optional_cats) > 0) {
            foreach ($scoutNetApi->get_categories_by_ids($optional_cats) as $optCategory) {
                $optional_categories[] = [
                    'selected' => in_array($optCategory->getUid(), $addcats),
                    'category' => $optCategory,
                ];
            }
        }


        return [
            '#type' => 'twig',
            '#theme' => 'snk_full_view',
            '#structures' => $structures,
            '#events' => $events,
            '#optionalStructures' => $optional_structures,
            '#categories' => $categories,
            '#optionalCategories' => $optional_categories,
//            '#eventId' => 80120, # make it possible to use this as get parameter
        ];
    }

    /**
     * Display the markup.
     *
     * @return array
     *   Return markup array.
     */
    public function event($eventId) {
        $scoutNetApi = new ScoutnetApi();
        $events = $scoutNetApi->get_events_with_ids(3, [intval($eventId)]); # SSID needs to be set, but is ignored

        if (count($events) != 1) {
            throw new NotFoundHttpException();
        }

        return [
            '#type' => 'twig',
            '#theme' => 'snk_event',
            '#event' => $events[0],
        ];
    }

}